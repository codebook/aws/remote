# AWS EC2 Remote Access (Debian/Ubuntu Instance)


_tbd_




## References

* [How can I connect to an Amazon EC2 Linux instance with desktop functionality from Windows?](https://aws.amazon.com/premiumsupport/knowledge-center/connect-to-linux-desktop-from-windows/)
* [Remote Desktop to an Amazon EC2 Instance](http://www.idevelopment.info/data/AWS/AWS_Tips/AWS_Management/AWS_12.shtml)
* [Installing the Qt-based X2Go Client](https://wiki.x2go.org/doku.php/doc:installation:x2goclient)
* [VNC/Servers](https://help.ubuntu.com/community/VNC/Servers)
* [How to Install and Configure VNC on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-vnc-on-ubuntu-16-04)


